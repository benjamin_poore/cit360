import java.util.TreeMap;
import java.io.*;
import java.util.ArrayList;
import java.util.Set;
import java.util.Iterator;

// Benjamin Poore - 05.04.18
//
// Open CSV file
// Read each row
// for each row:
// read line
// split line
// check if accountNumber is NOT in the TreeMap (use methods in TreeMap)
// Create Account object
// Put object in TreeMap

// for each loop to display

// output: Info for each unique account


public class Account {

    private String accountNumber;
    private String accountType;
    private String givenName;
    private String familyName;


    public Account(String accountNumber, String accountType, String givenName, String familyName) {
        this.accountNumber = accountNumber;
        this.accountType = accountType;
        this.givenName = givenName;
        this.familyName = familyName;
    }

    public static void main(String args[]) {

        File rawData = new File("rawData.csv");

        // declare new TreeMap called treeMap1
        TreeMap<String, Account> treeMap1 = new TreeMap<>();

        try {

            // read text file
            String[] read = rText(rawData);

            for (String line : read) {

                String[] dSplit = line.split(",");
                if (!treeMap1.containsKey(dSplit[0])) {
                    Account tempAccount = new Account(dSplit[0], dSplit[1], dSplit[2], dSplit[3]);
                    // integer key, account value
                    treeMap1.put(dSplit[0], tempAccount);
                }
            }

            Set keys = treeMap1.keySet();

            for (Iterator i = keys.iterator(); i.hasNext();) {

                String key = (String) i.next();
                Account value = treeMap1.get(key);
                System.out.println(key + " = " + value.accountType + " - " + value.familyName + ", " + value.givenName);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

    }
        public static String[] rText(File rawData) throws IOException {

            ArrayList<String> textArray = new ArrayList<>();


            // read text file using BufferedReader
            BufferedReader buffRead = null;
            FileReader fileRead = new FileReader(rawData);
            try {
                buffRead = new BufferedReader(fileRead);

                // read all of the lines in the rawData.csv file
                String line;
                buffRead.readLine();
                while ((line = buffRead.readLine()) != null) {
                    textArray.add(line);
                }

            }
            finally {
                if (buffRead != null) {
                    buffRead.close();

                }
                else {
                    fileRead.close();
                }
            }

            int size = textArray.size();
            String[] array = new String[size];
            return textArray.toArray(array);

        }


    }
