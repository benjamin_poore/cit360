
// extends takes the attributes from MammalBean
public class DogBean extends MammalBean {

    private String breed;
    private String name;


    //getters
    public String getBreed() {
        return breed;
    }

    public String getName() {
        return name;
    }


    //setters
    public void setBreed(String breed) {
        this.breed = breed;
    }

    public void setName(String name) {
        this.name = name;
    }

    //constructor
    //super refers to MammalBean class (one level up of inheritance)
    public DogBean(int legCount, String color, double height, String breed, String name) {
        super(legCount, color, height);
        this.breed = breed;
        this.name = name;
    }

    //toString

    // @Override is not required, it's just describing to the compiler what you expect to happen.
    // a class that does not extend anything automatically extends the class "Object".
    // We put @Override to override the toString that's in the "Object" class so it uses ours below instead.
    // @Override is a best practice that will help you to catch if you intend to have it override, but it's not because you have added a parameter
    @Override
    public String toString() {
        return "DogBean{" +
                "breed: " + breed +
                ", name: " + name +
                ", height: " + this.getHeight() +
                ", color: " + this.getColor() +
                ", legCount: " + this.getLegCount() +
                '}';
    }
}
