import org.junit.Test;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import static org.junit.Assert.*;

public class MammalBeanTest {


    // or 1e-6
    // static means belongs to the class, only one copy of delta no matter how many mammals are created
    //
    private static final double delta = 0.000001;


    @Test
    public void testMammal() {
        MammalBean mammal_1 = new MammalBean(1, "red", 1.1);
        MammalBean mammal_2 = new MammalBean(2, "yellow", 1.2);
        MammalBean mammal_3 = new MammalBean(3, "blue", 1.3);
        MammalBean mammal_4 = new MammalBean(4, "green", 1.4);
        MammalBean mammal_5 = new MammalBean(5, "purple", 1.5);

        // Assert that all the objects hold the correct values.

        // test mammal expected first, actual second

        // test mammal 1
        assertEquals(1, mammal_1.getLegCount());
        assertEquals("red", mammal_1.getColor());
        // when using float point numbers (approximation for double, so it will not come out right)
        assertEquals(1.1, mammal_1.getHeight(), delta);

        // test mammal 2
        assertEquals(2, mammal_2.getLegCount());
        assertEquals("yellow", mammal_2.getColor());
        assertEquals(1.2, mammal_2.getHeight(), delta);

        // test mammal 3
        assertEquals(3, mammal_3.getLegCount());
        assertEquals("blue", mammal_3.getColor());
        assertEquals(1.3, mammal_3.getHeight(), delta);

        // test mammal 4
        assertEquals(4, mammal_4.getLegCount());
        assertEquals("green", mammal_4.getColor());
        assertEquals(1.4, mammal_4.getHeight(), delta);

        // test mammal 5
        assertEquals(5, mammal_5.getLegCount());
        assertEquals("purple", mammal_5.getColor());
        assertEquals(1.5, mammal_5.getHeight(), delta);

        // create a set of Mammal objects
        Set mammalSet = new HashSet();

        // add mammal objects to set

        mammalSet.add(mammal_1);
        mammalSet.add(mammal_2);
        mammalSet.add(mammal_3);
        mammalSet.add(mammal_4);
        mammalSet.add(mammal_5);

        // initial assertion tests
        assertEquals(5, mammalSet.size());
        assertEquals(true, mammalSet.contains(mammal_1));
        assertEquals(true, mammalSet.contains(mammal_2));
        assertEquals(true, mammalSet.contains(mammal_3));
        assertEquals(true, mammalSet.contains(mammal_4));
        assertEquals(true, mammalSet.contains(mammal_5));

        // remove a few objects from set
        mammalSet.remove(mammal_2);
        mammalSet.remove(mammal_4);

        // assertions to ensure that the correct mammal objects have been removed
        assertEquals(false, mammalSet.contains(mammal_2));
        assertEquals(false, mammalSet.contains(mammal_4));

        // assertions to ensure that the correct mammal objects remain
        assertEquals(true, mammalSet.contains(mammal_1));
        assertEquals(true, mammalSet.contains(mammal_3));
        assertEquals(true, mammalSet.contains(mammal_5));

    }

    @Test
    public void testDog() {

        DogBean dog_1 = new DogBean(1, "green", 1.0, "Mut", "Bolt");
        DogBean dog_2 = new DogBean(1, "green", 1.0, "Mut", "Pascow");
        DogBean dog_3 = new DogBean(1, "green", 1.0, "Mut", "Flynn");
        DogBean dog_4 = new DogBean(1, "green", 1.0, "Mut", "Maximus");
        DogBean dog_5 = new DogBean(1, "green", 1.0, "Mut", "Rapunzel");


        // test Dogs

        // dog 1
        assertEquals(1, dog_1.getLegCount());
        assertEquals("green", dog_1.getColor());
        assertEquals(1.0, dog_1.getHeight(), delta);
        assertEquals("Mut", dog_1.getBreed());
        assertEquals("Bolt", dog_1.getName());

        // dog 2
        assertEquals(1, dog_2.getLegCount());
        assertEquals("green", dog_2.getColor());
        assertEquals(1.0, dog_2.getHeight(), delta);
        assertEquals("Mut", dog_2.getBreed());
        assertEquals("Pascow", dog_2.getName());

        // dog 3
        assertEquals(1, dog_3.getLegCount());
        assertEquals("green", dog_3.getColor());
        assertEquals(1.0, dog_3.getHeight(), delta);
        assertEquals("Mut", dog_3.getBreed());
        assertEquals("Flynn", dog_3.getName());

        // dog 4
        assertEquals(1, dog_4.getLegCount());
        assertEquals("green", dog_4.getColor());
        assertEquals(1.0, dog_4.getHeight(), delta);
        assertEquals("Mut", dog_4.getBreed());
        assertEquals("Maximus", dog_4.getName());

        // dog 5
        assertEquals(1, dog_5.getLegCount());
        assertEquals("green", dog_5.getColor());
        assertEquals(1.0, dog_5.getHeight(), delta);
        assertEquals("Mut", dog_5.getBreed());
        assertEquals("Rapunzel", dog_5.getName());


        // create a map of Mammal objects
        HashMap<String, DogBean>dogMap = new HashMap<>();

        // add dog objects to map
        dogMap.put("Bolt", dog_1);
        dogMap.put("Pascow", dog_2);
        dogMap.put("Flynn", dog_3);
        dogMap.put("Maximus", dog_4);
        dogMap.put("Rapunzel", dog_5);

        // initial assertion tests
        assertEquals(5, dogMap.size());
        assertEquals(true, dogMap.containsValue(dog_1));
        assertEquals(true, dogMap.containsValue(dog_2));
        assertEquals(true, dogMap.containsValue(dog_3));
        assertEquals(true, dogMap.containsValue(dog_4));
        assertEquals(true, dogMap.containsValue(dog_5));

        // remove a few objects from set
        dogMap.remove("Bolt", dog_1);
        dogMap.remove("Rapunzel", dog_5);

        // assertions to ensure that the correct mammal objects have been removed
        assertEquals(false, dogMap.containsValue(dog_1));
        assertEquals(false, dogMap.containsValue(dog_5));

        // assertions to ensure that the correct mammal objects remain
        assertEquals(true, dogMap.containsValue(dog_2));
        assertEquals(true, dogMap.containsValue(dog_3));
        assertEquals(true, dogMap.containsValue(dog_4));

        }
    }
