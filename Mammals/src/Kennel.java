public class Kennel {

    //method named buildDogs that will return an array "DogBean"
    public DogBean[] buildDogs() {

        DogBean dog_1 = new DogBean(4, "black", 5, "Laborador", "Korra");
        DogBean dog_2 = new DogBean(4, "brown", 4.3, "Irish Setter", "Roy");
        DogBean dog_3 = new DogBean(4, "golden yellow", 2, "Golden Retriever", "Neon");
        DogBean dog_4 = new DogBean(3, "white", 1.7, "Animated Dog", "Bolt");
        DogBean dog_5 = new DogBean(4, "green", 0.5, "Poodle", "Pascow");

        DogBean[] dogArray = {dog_1, dog_2, dog_3, dog_4, dog_5};

        return dogArray;

    }

// B. “displayDogs”
    // This non-static method has one parameter—an array of DogBeans.
    // It iterates over the array it is passed and calls the custom
    // toString method for each DogBean.

    public void displayDogs(DogBean[] dogBean) {

        for (int i = 0;  i < dogBean.length;  ++i) {

            //toString does not take any parameters in parenthesis
            System.out.println(dogBean[i].toString());
        }
    }

    // “main”
    // This is the standard static main method.
    // In this method you must call buildDogs and store the result in a DogBeans array named “allDogs” and
    // call displayDogs and pass it the allDogs array reference

public static void main(String args[]) {

        Kennel Kennel_1 = new Kennel();
        DogBean[] allDogs = Kennel_1.buildDogs();
    // object."do something"
        Kennel_1.displayDogs(allDogs);


}



}


