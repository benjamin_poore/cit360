import java.util.HashMap;

public class CollectionTest {

    public static void main(String[] args) {

        // key = name, value = age

        HashMap<String, Integer> people = new HashMap<>();

        people.put("Gabriel", 22);
        people.put("Dan", 25);
        people.put("Kailen", 22);
        // keys are unique so the second Kailen actually replaces the first one
        people.put("Kailen", 21);


        System.out.println(people);

        Integer i = people.get("Kailen");
                System.out.println(i);


    }


    // for assignment: Account number is key, other information are values
    // take a look at example code in "FileIO"
    // read file, create collection


}



// import java.util.ArrayList;
//
//public class CollectionTest {
//
//    public static void main(String[] args) {
//
//        ArrayList<String> names = new ArrayList<>();
//
//        names.add("Gabriel");
//        names.add("Dan");
//        names.add("Kailen");
//
//        System.out.println(names);
//
//
//
//
//    }
//
//
//
//
//
//}