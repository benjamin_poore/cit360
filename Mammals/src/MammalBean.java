public class MammalBean {

    //Write a Java class named “MammalBean”.
    //This class must have the following private properties:
    //legCount - type int
    //color - type String
    //height - type double
    //The MammalBean class must have a constructor that accepts and sets values for each of the three properties.
    //The MammalBean class must have getter and setter methods for each of the properties.

    // If any of these are made static, then whatever was last set will stay. Only one variable, instead of public where each have their own variable.
    // For example, if legCount was static, the last set legCount would stand, so all objects would have the same legCount
    // Closest thing that Java has to a global variable is a static variable
    private int legCount;
    private String color;
    private double height;

    //constructor

    public MammalBean(int legCount, String color, double height) {
        this.legCount = legCount;
        this.color = color;
        this.height = height;
    }


    //getters
    public int getLegCount() {
        return legCount;
    }

    public String getColor() {
        return color;
    }

    public double getHeight() {
        return height;
    }


    //setters   (add underscore before legCount to differentiate it.)
    public void setLegCount(int legCount) {
        this.legCount = legCount;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setHeight(double height) {
        this.height = height;
    }
}


